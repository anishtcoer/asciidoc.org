# Heading 1

A paragraph with **bold** and *italic* text.
A link to [Eclipse](https://eclipse.org).
A reusable link to [GitLab](gitlab).

![An image](an-image.png)

## Heading 2

* Unordered list item
  * Nest items by aligning marker with text of parent item
* Another unordered list item

**NOTE:** An admonition can be emulated using a bold label.

### Heading 3

    Text indented by four spaces is preformatted.

A code block with a Ruby function named `hello` that prints “Hello, World!”:

```ruby
def hello name = 'World'
  puts "Hello, #{name}!"
end
```

[gitlab]: https://gitlab.com
